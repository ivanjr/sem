import pygame
import time
import os
import random

pygame.font.init()
WIDTH, HEIGHT = 1280, 720
SCREEN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Space Invaders")
# Lode
RED_SHIP = pygame.image.load(os.path.join("assets", "redship.png"))
GREEN_SHIP = pygame.image.load(os.path.join("assets", "greenship.png"))
BLUE_SHIP = pygame.image.load(os.path.join("assets", "blueship.png"))
# Hrac
YELLOW_SHIP = pygame.image.load(os.path.join("assets", "yellowship.png"))
# Lasery
RED_LASER = pygame.image.load(os.path.join("assets", "redlaser.png"))
GREEN_LASER = pygame.image.load(os.path.join("assets", "greenlaser.png"))
BLUE_LASER = pygame.image.load(os.path.join("assets", "bluelaser.png"))
YELLOW_LASER = pygame.image.load(os.path.join("assets", "yellowlaser.png"))
# Pozadi
BG = pygame.transform.scale(pygame.image.load(os.path.join("assets", "blackbag.png")), (WIDTH, HEIGHT))
class Laser:
    def __init__(self, x, y, img):
        self.x = x
        self.y = y
        self.img = img
        self.mask = pygame.mask.from_surface(self.img)
    def draw(self, window):
        window.blit(self.img, (self.x, self.y))

    def move(self, mov):
        self.y += mov

    def not_on_screen(self, height):
        return not(self.y <= height and self.y >= 0)

    def collision(self, obj):
        return collide(self, obj)

class Ship:
    def __init__(self, x, y, health=100,cool = 30):
        self.x = x
        self.y = y
        self.health = health
        self.ship_img = None
        self.laser_img = None
        self.lasers = []
        self.cool_down_counter = 0
        self.COOLDOWN = cool
        self.fast_cool = 120
        self.currentcool = 0
        self.duration = 0
    def draw(self, window):
        window.blit(self.ship_img, (self.x, self.y))
        for laser in self.lasers:
            laser.draw(window)
    def move_lasers(self, mov, obj):
        self.cooldown()
        for laser in self.lasers:
            laser.move(mov)
            if laser.not_on_screen(HEIGHT):
                self.lasers.remove(laser)
            elif laser.collision(obj):
                obj.health -= 10
                self.lasers.remove(laser)
    def fastlaser(self,activate):
        if self.currentcool == 0 and activate == 1:
            self.currentcool = self.fast_cool
            self.COOLDOWN = 5
            self.duration = 60
        if activate == 0:
            if self.currentcool>0:
                self.currentcool += -1
            self.duration += -1
            if self.duration  < 1:
                self.duration = 0
                self.COOLDOWN = 30
    def cooldown(self):
        if self.cool_down_counter >= self.COOLDOWN:
            self.cool_down_counter = 0  
        elif self.cool_down_counter > 0: 
            self.cool_down_counter += 1

    def shoot(self):
        if self.cool_down_counter == 0:
            laser = Laser(self.x, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cool_down_counter = 1

    def get_width(self):
        return self.ship_img.get_width()
        
    def get_height(self):
        return self.ship_img.get_height()

class Player(Ship):
    def __init__(self, x, y, health=100):
        super().__init__(x, y, health)
        self.ship_img = YELLOW_SHIP
        self.laser_img = YELLOW_LASER
        self.mask = pygame.mask.from_surface(self.ship_img)
        self.max_health = health
    def move_lasers(self, mov, objs):
        self.cooldown()
        self.fastlaser(0)
        for laser in self.lasers:
            laser.move(mov)
            if laser.not_on_screen(HEIGHT):
                self.lasers.remove(laser)
            else:
                for obj in objs:
                    if laser.collision(obj):
                        objs.remove(obj)
                        if laser in self.lasers:
                            self.lasers.remove(laser)

    def draw(self, window):
        super().draw(window)
        self.hb(window)
    def hb(self, window):
        pygame.draw.rect(window, (255,0,0), (self.x, self.y + self.ship_img.get_height() + 10, self.ship_img.get_width(), 10))
        pygame.draw.rect(window, (0,255,0), (self.x, self.y + self.ship_img.get_height() + 10, self.ship_img.get_width() * (self.health/self.max_health), 10))

class Enemy(Ship):
    Barvy = {
                "red": (RED_SHIP, RED_LASER),
                "green": (GREEN_SHIP, GREEN_LASER),
                "blue": (BLUE_SHIP, BLUE_LASER)
                }

    def __init__(self, x, y, color, health=100):
        super().__init__(x, y, health)
        self.ship_img, self.laser_img = self.Barvy[color]
        self.mask = pygame.mask.from_surface(self.ship_img)

    def move(self, mov):
        self.y += mov
   
    def shoot(self):
        if self.cool_down_counter == 0:
            laser = Laser(self.x-18, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cool_down_counter = 1

def collide(obj1, obj2):
    mask_x = obj2.x - obj1.x
    mask_y = obj2.y - obj1.y
    return obj1.mask.overlap(obj2.mask, (mask_x, mask_y)) != None

def main():

    run = True
    FPS = 60
    lemov = 0
    lives = 5
    main_font = pygame.font.SysFont("arial", 40)
    lost_font = pygame.font.SysFont("arial", 50)
    enemies = []
    wave_length = 5
    enemy_mov = 1
    player_mov = 5
    laser_mov = 5
    player = Player(300, 600)
    clock = pygame.time.Clock()
    lost = False
    lost_count = 0

    def redraw_window():
        SCREEN.blit(BG, (0,0))
        lemov_label = main_font.render(f"Úroveň: {lemov}", 1, (255,215,0))
        SCREEN.blit(lemov_label, (WIDTH - lemov_label.get_width() - 20, 20))
        for enemy in enemies:
            enemy.draw(SCREEN)
        player.draw(SCREEN)
        if lost:
            lost_label = lost_font.render("ZKUS TO ZNOVU!", 1, (255,215,0))
            SCREEN.blit(lost_label, (WIDTH/2 - lost_label.get_width()/2, 350))
        pygame.display.update()        
    while run:
        clock.tick(FPS)
        redraw_window()
        if lives <= 0 or player.health <= 0:
            lost = True
            lost_count += 1
        if lost:
            if lost_count > FPS * 3:
                run = False
            else:
                continue
        if len(enemies) == 0:
            lemov += 1
            wave_length += 5
            for i in range(wave_length):
                enemy = Enemy(random.randrange(50, WIDTH-100), random.randrange(-1500, -100), random.choice(["red", "blue", "green"]))
                enemies.append(enemy)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()
        keys = pygame.key.get_pressed()
        if keys[pygame.K_a] and player.x - player_mov > 0: # Vlevo
            player.x -= player_mov
        if keys[pygame.K_d] and player.x + player_mov + player.get_width() < WIDTH: # Vpravo
            player.x += player_mov
        if keys[pygame.K_w] and player.y - player_mov > 0: # Nahoru
            player.y -= player_mov
        if keys[pygame.K_s] and player.y + player_mov + player.get_height() + 25 < HEIGHT: # Dolu
            player.y += player_mov
        if keys[pygame.K_f] : # Fast laser
            player.fastlaser(1 )
        if keys[pygame.K_SPACE]:
            player.shoot()
        for enemy in enemies[:]:
            enemy.move(enemy_mov)
            enemy.move_lasers(laser_mov, player)
            if random.randrange(0, 120) == 1:
                enemy.shoot()
            if collide(enemy, player):
                player.health -= 10
                enemies.remove(enemy)
            elif enemy.y + enemy.get_height() > HEIGHT:
                lives -= 1
                enemies.remove(enemy)
        player.move_lasers(-laser_mov, enemies)

def main_menu():
    title_font = pygame.font.SysFont("arial", 50)
    run = True
    while run:
        SCREEN.blit(BG, (0,0))
        uvod = title_font.render("STISKNI TLAČÍTKO MYŠI, MÁŠ 5 ŽIVOTŮ", 1, (255,215,0))
        SCREEN.blit(uvod, (WIDTH/2 - uvod.get_width()/2, 350))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                main()

    pygame.quit()

main_menu()